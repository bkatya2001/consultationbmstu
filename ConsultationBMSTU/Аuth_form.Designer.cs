﻿namespace ConsultationBMSTU
{
    partial class Auth_form
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Auth_form));
            this.auth_btn = new System.Windows.Forms.Button();
            this.password_lbl = new System.Windows.Forms.Label();
            this.login_lbl = new System.Windows.Forms.Label();
            this.password_tb = new System.Windows.Forms.TextBox();
            this.login_tb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // auth_btn
            // 
            this.auth_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.auth_btn.Font = new System.Drawing.Font("Courier New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.auth_btn.Location = new System.Drawing.Point(125, 300);
            this.auth_btn.Name = "auth_btn";
            this.auth_btn.Size = new System.Drawing.Size(250, 80);
            this.auth_btn.TabIndex = 0;
            this.auth_btn.Text = "Войти";
            this.auth_btn.UseVisualStyleBackColor = false;
            this.auth_btn.Click += new System.EventHandler(this.auth_btn_Click);
            // 
            // password_lbl
            // 
            this.password_lbl.AutoSize = true;
            this.password_lbl.Font = new System.Drawing.Font("Courier New", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.password_lbl.Location = new System.Drawing.Point(14, 172);
            this.password_lbl.Name = "password_lbl";
            this.password_lbl.Size = new System.Drawing.Size(133, 31);
            this.password_lbl.TabIndex = 2;
            this.password_lbl.Text = "Пароль:";
            // 
            // login_lbl
            // 
            this.login_lbl.AutoSize = true;
            this.login_lbl.Font = new System.Drawing.Font("Courier New", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.login_lbl.Location = new System.Drawing.Point(14, 64);
            this.login_lbl.Name = "login_lbl";
            this.login_lbl.Size = new System.Drawing.Size(116, 31);
            this.login_lbl.TabIndex = 3;
            this.login_lbl.Text = "Логин:";
            // 
            // password_tb
            // 
            this.password_tb.Location = new System.Drawing.Point(166, 177);
            this.password_tb.Name = "password_tb";
            this.password_tb.PasswordChar = '*';
            this.password_tb.Size = new System.Drawing.Size(300, 26);
            this.password_tb.TabIndex = 4;
            // 
            // login_tb
            // 
            this.login_tb.Location = new System.Drawing.Point(166, 69);
            this.login_tb.Name = "login_tb";
            this.login_tb.Size = new System.Drawing.Size(300, 26);
            this.login_tb.TabIndex = 5;
            // 
            // Auth_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(478, 444);
            this.Controls.Add(this.login_tb);
            this.Controls.Add(this.password_tb);
            this.Controls.Add(this.login_lbl);
            this.Controls.Add(this.password_lbl);
            this.Controls.Add(this.auth_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "Auth_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизация";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button auth_btn;
        private System.Windows.Forms.Label password_lbl;
        private System.Windows.Forms.Label login_lbl;
        private System.Windows.Forms.TextBox password_tb;
        private System.Windows.Forms.TextBox login_tb;
    }
}

