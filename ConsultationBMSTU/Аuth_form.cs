﻿using ConsultationBMSTU.UserClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    public partial class Auth_form : Form
    {
        public Auth_form()
        {
            InitializeComponent();
        }

        private void auth_btn_Click(object sender, EventArgs e)
        {
            string login = login_tb.Text;
            string password = password_tb.Text;
            login_tb.Clear();
            password_tb.Clear();
            List<string> data = Program.database.GetStudent(login, password);
            if (data.Count == 0)
            {
                data = Program.database.GetTeacher(login, password);
                if (data.Count == 0)
                {
                    MessageBox.Show("Неверный логин или пароль", "Ошибка входа", MessageBoxButtons.OK);
                }
                else
                {
                    Program.teacher = new Teacher(data[1], data[2], data[3], Convert.ToInt32(data[0]));
                    TeacherMenu_form tmf = new TeacherMenu_form(this);
                    tmf.Show();
                    this.Hide();
                }
            }
            else
            {
                Program.student = new Student(data[1], data[2], Convert.ToInt32(data[0]), Convert.ToInt32(data[3]), Convert.ToInt32(data[4]), data[5]);
                StudentMenu_form smf = new StudentMenu_form(this);
                smf.Show();
                this.Hide();
            }
        }
    }
}
