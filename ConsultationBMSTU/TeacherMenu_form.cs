﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    public partial class TeacherMenu_form : Form
    {
        Auth_form auth_Form;
        public TeacherMenu_form()
        {
            InitializeComponent();
        }

        public TeacherMenu_form(Auth_form _auth_Form)
        {
            InitializeComponent();
            auth_Form = _auth_Form;
        }

        private void do_btn_Click(object sender, EventArgs e)
        {
            Create_form cf = new Create_form(this);
            cf.Show();
            this.Hide();
        }

        private void show_btn_Click(object sender, EventArgs e)
        {
            Consultation_form cf = new Consultation_form(this);
            cf.Show();
            this.Hide();
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            auth_Form.Show();
            this.Close();
        }

        private void TeacherMenu_form_Load(object sender, EventArgs e) { }
    }
}
