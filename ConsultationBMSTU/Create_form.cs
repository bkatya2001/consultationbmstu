﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ConsultationBMSTU
{
    public partial class Create_form : Form
    {
        private List<string> groups;
        TeacherMenu_form form;
        public Create_form()
        {
            InitializeComponent();
        }

        public Create_form(TeacherMenu_form _form)
        {
            InitializeComponent();
            form = _form;
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            form.Show();
            this.Close();
        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            List<int> id = new List<int>();
            try
            {
                DateTime date = DateTime.Parse(date_mtb.Text);
                DateTime time = DateTime.Parse(time_mtb.Text);
                DateTime result = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, 0);
                switch (repeat_cb.SelectedIndex)
                {
                    case 0:
                        id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, result, Convert.ToInt32(duration_tb.Text)));
                        break;
                    case 1:
                        if (DateTime.Now.Month >= 9 || DateTime.Now.Month <= 1)
                        {
                            DateTime dateTime = result;
                            while (dateTime.Day <= 31 && dateTime.Month <= 1)
                            {
                                id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, dateTime, Convert.ToInt32(duration_tb.Text)));
                                dateTime = dateTime.AddDays(7);
                            }
                        }
                        else
                        {
                            DateTime dateTime = result;
                            while (dateTime.Day <= 30 && dateTime.Month <= 6)
                            {
                                id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, dateTime, Convert.ToInt32(duration_tb.Text)));
                                dateTime = dateTime.AddDays(7);
                            }
                        }
                        break;
                    case 2:
                        if (DateTime.Now.Month >= 9 || DateTime.Now.Month <= 1)
                        {
                            DateTime dateTime = result;
                            while (dateTime.Day <= 31 && dateTime.Month <= 1)
                            {
                                id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, dateTime, Convert.ToInt32(duration_tb.Text)));
                                dateTime = dateTime.AddDays(14);
                            }
                        }
                        else
                        {
                            DateTime dateTime = result;
                            while (dateTime.Day <= 31 && dateTime.Month <= 5)
                            {
                                id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, dateTime, Convert.ToInt32(duration_tb.Text)));
                                dateTime = dateTime.AddDays(14);
                            }
                        }
                        break;
                    case 3:
                        if (DateTime.Now.Month >= 9 || DateTime.Now.Month <= 1)
                        {
                            DateTime dateTime = result;
                            while (dateTime.Day <= 31 && dateTime.Month <= 1)
                            {
                                id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, dateTime, Convert.ToInt32(duration_tb.Text)));
                                dateTime = dateTime.AddDays(28);
                            }
                        }
                        else
                        {
                            DateTime dateTime = result;
                            while (dateTime.Day <= 31 && dateTime.Month <= 5)
                            {
                                id.Add(Program.database.AddConsultation(Program.teacher.id, subject_tb.Text, dateTime, Convert.ToInt32(duration_tb.Text)));
                                dateTime = dateTime.AddDays(28);
                            }
                        }
                        break;
                }
                for (int i = 0; i < group_lb.Items.Count; i++)
                {
                    if (group_lb.GetSelected(i) == true)
                    {
                        foreach (var item in id) Program.database.AddTimetable(item, groups[i].Split('-')[0], groups[i].Split('-')[1].Replace("Б", ""));
                    }
                }
                MessageBox.Show("Консультации успешно добавлены в расписание", "Информация", MessageBoxButtons.OK);
                subject_tb.Clear();
                date_mtb.Clear();
                time_mtb.Clear();
                duration_tb.Text = "Минуты";
                repeat_cb.ClearSelected();
                group_lb.ClearSelected();
            }
            catch
            {
                MessageBox.Show("Неверный формат даты или времени", "Ошибка ввода", MessageBoxButtons.OK);
            }
        }

        private void Create_form_Load(object sender, EventArgs e) 
        {
            group_lb.Items.Clear();
            groups = Program.database.GetGroups();
            foreach (var g in groups) group_lb.Items.Add(g);
        }
    }
}
