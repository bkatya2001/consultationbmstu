﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    public partial class Records_form : Form
    {
        StudentMenu_form form;
        public Records_form()
        {
            InitializeComponent();
        }

        public Records_form(StudentMenu_form _form)
        {
            InitializeComponent();
            form = _form;
        }

        private void UpdateTable()
        {
            List<string> records = Program.database.GetStudentRecords(Program.student.id);
            foreach (var r in records)
            {
                var arr = r.Split('_');
                table.Rows.Add(arr[0], arr[1], arr[2], arr[3].Remove(5), arr[4]);
            }
        }

        private void Records_form_Load(object sender, EventArgs e)
        {
            var subject_col = new DataGridViewColumn();
            var teacher_col = new DataGridViewColumn();
            var date_col = new DataGridViewColumn();
            var time_col = new DataGridViewColumn();
            var id_col = new DataGridViewColumn();
            subject_col.HeaderText = "Дисциплина";
            subject_col.CellTemplate = new DataGridViewTextBoxCell();
            teacher_col.HeaderText = "Преподаватель";
            teacher_col.CellTemplate = new DataGridViewTextBoxCell();
            date_col.HeaderText = "Дата";
            date_col.CellTemplate = new DataGridViewTextBoxCell();
            time_col.HeaderText = "Время";
            time_col.CellTemplate = new DataGridViewTextBoxCell();
            id_col.Visible = false;
            id_col.CellTemplate = new DataGridViewTextBoxCell();
            table.Columns.Add(subject_col);
            table.Columns.Add(teacher_col);
            table.Columns.Add(date_col);
            table.Columns.Add(time_col);
            table.Columns.Add(id_col);
            table.AllowUserToAddRows = false;
            UpdateTable();
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            if (table.CurrentRow != null)
            {
                var result = MessageBox.Show("Вы действительно хотите удалить запись?", "Удаление", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Program.database.DeleteRecord(Convert.ToInt32(table.Rows[table.CurrentRow.Index].Cells[4].Value), Program.student.id);
                    MessageBox.Show("Запись удалена.", "Удаление", MessageBoxButtons.OK);
                    UpdateTable();
                }
            }
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            form.Show();
            this.Close();
        }
    }
}
