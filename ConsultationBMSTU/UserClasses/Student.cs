﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsultationBMSTU.UserClasses
{
    public class Student : User
    {
        public int group_num;
        public int year;
        public string faculty;

        public Student(string _surname, string _name, int _id, int _group_num, int _year, string _faculty) : base(_surname, _name, _id)
        {
            group_num = _group_num;
            year = _year;
            faculty = _faculty;
        }
    }
}
