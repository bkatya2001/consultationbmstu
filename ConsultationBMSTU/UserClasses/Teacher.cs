﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultationBMSTU.UserClasses
{
    public class Teacher : User
    {
        public string patronymic;

        public Teacher(string _surname, string _name, string _patronymic, int _id) : base(_surname, _name, _id)
        {
            patronymic = _patronymic;
        }
    }
}
