﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultationBMSTU.UserClasses
{
    public class User
    {
        public string surname;
        public string name;
        public int id;

        public User(string _surname, string _name, int _id)
        {
            surname = _surname;
            name = _name;
            id = _id;
        }
    }
}
