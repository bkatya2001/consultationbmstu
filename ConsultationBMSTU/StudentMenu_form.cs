﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    public partial class StudentMenu_form : Form
    {
        Auth_form auth_Form;
        public StudentMenu_form()
        {
            InitializeComponent();
        }

        public StudentMenu_form(Auth_form _auth_Form)
        {
            InitializeComponent();
            auth_Form = _auth_Form;
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            auth_Form.Show();
            this.Close();
        }

        private void do_btn_Click(object sender, EventArgs e)
        {
            StudentList_form slf = new StudentList_form(this);
            slf.Show();
            this.Hide();
        }

        private void show_btn_Click(object sender, EventArgs e)
        {
            Records_form rf = new Records_form(this);
            rf.Show();
            this.Hide();
        }

        private void StudentMenu_form_Load(object sender, EventArgs e) { }
    }
}
