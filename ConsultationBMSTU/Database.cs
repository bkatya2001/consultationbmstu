﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsultationBMSTU
{
    partial class ConsultationBMSTUDataSet
    {
        private string connectionString = "Data Source=LAPTOP-K1DGUPRH\\SQLEXPRESS;Initial Catalog=ConsultationBMSTU;Integrated Security=True";

        public List<string> GetStudent(string login, string password)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id, surname, name, group_num, year, faculty FROM Student WHERE login = @login AND password = @password";
                    comm.Parameters.AddWithValue("@login", login);
                    comm.Parameters.AddWithValue("@password", password);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString());
                        result.Add(r[1].ToString());
                        result.Add(r[2].ToString());
                        result.Add(r[3].ToString());
                        result.Add(r[4].ToString());
                        result.Add(r[5].ToString());
                        break;
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public List<string> GetTeacher(string login, string password)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id, surname, name, patronymic FROM Teacher WHERE login = @login AND password = @password";
                    comm.Parameters.AddWithValue("@login", login);
                    comm.Parameters.AddWithValue("@password", password);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString());
                        result.Add(r[1].ToString());
                        result.Add(r[2].ToString());
                        result.Add(r[3].ToString());
                        break;
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public List<string> GetGroups()
        {
            List<string> result = new List<string>();
            int sem;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT DISTINCT faculty, year, group_num FROM Student ORDER BY faculty, year, group_num";
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        sem = Program.GetSem(Convert.ToInt32(r[1]));
                        result.Add(r[0].ToString() + "-" + sem.ToString() + r[2].ToString() + "Б");
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public int AddConsultation(int id_teacher, string subject, DateTime dateTime, int duration)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "INSERT INTO Consultation VALUES(@id_teacher, @subject, @dateTime, @duration)";
                    comm.Parameters.AddWithValue("@id_teacher", id_teacher);
                    comm.Parameters.AddWithValue("@subject", subject);
                    comm.Parameters.AddWithValue("@dateTime", dateTime);
                    comm.Parameters.AddWithValue("@duration", duration);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    comm.CommandText = "SELECT id FROM Consultation WHERE id_teacher = @id_teacher AND subject = @subject AND date_time = @dateTime AND duration_minutes = @duration";
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result = Convert.ToInt32(r[0]);
                        break;
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public void AddTimetable(int id_consultation, string faculty, string grade)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "INSERT INTO Timetable VALUES (@id_consultation, @faculty, @grade)";
                    comm.Parameters.AddWithValue("@id_consultation", id_consultation);
                    comm.Parameters.AddWithValue("@faculty", faculty);
                    comm.Parameters.AddWithValue("@grade", grade);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public List<string> GetSubjects(string faculty, int grade)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT DISTINCT subject FROM Consultation WHERE id IN (SELECT id_consultation FROM Timetable WHERE faculty = @faculty AND grade = @grade)";
                    comm.Parameters.AddWithValue("@faculty", faculty);
                    comm.Parameters.AddWithValue("@grade", grade);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString());
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public List<string> GetTeachers(string subject, string faculty, int grade)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id, surname, name, patronymic FROM Teacher WHERE id IN (SELECT id_teacher FROM Consultation WHERE subject = @subject AND id IN (SELECT id_consultation FROM Timetable WHERE faculty = @faculty AND grade = @grade))";
                    comm.Parameters.AddWithValue("@subject", subject);
                    comm.Parameters.AddWithValue("@faculty", faculty);
                    comm.Parameters.AddWithValue("@grade", grade);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString() + '-' + r[1].ToString() + ' ' + r[2].ToString() + ' ' + r[3].ToString());
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public List<string> GetDates(int id, string subject, string faculty, int grade)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id, date_time, duration_minutes FROM Consultation WHERE id_teacher = @id AND subject = @subject AND id IN (SELECT id_consultation FROM Timetable WHERE faculty = @faculty AND grade = @grade)";
                    comm.Parameters.AddWithValue("@id", id);
                    comm.Parameters.AddWithValue("@subject", subject);
                    comm.Parameters.AddWithValue("@faculty", faculty);
                    comm.Parameters.AddWithValue("@grade", grade);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString() + '_' + r[1].ToString() + '_' + r[2].ToString());
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public List<string> GetRecords(int id)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT start_time, duration_minutes FROM Record WHERE id_consultation = @id ORDER BY start_time";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString() + '_' + r[1].ToString());
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public void AddRecord(int id_consultation, int id_student, DateTime time, int duration)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "INSERT INTO Record VALUES (@id_consultation, @id_student, @time, @duration)";
                    comm.Parameters.AddWithValue("@id_consultation", id_consultation);
                    comm.Parameters.AddWithValue("@id_student", id_student);
                    comm.Parameters.AddWithValue("@time", time.TimeOfDay);
                    comm.Parameters.AddWithValue("@duration", duration);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public bool HasRecord(int id_consultation, int id_student)
        {
            bool result = false;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id FROM Record WHERE id_consultation = @id_consultation AND id_student = @id_student";
                    comm.Parameters.AddWithValue("@id_consultation", id_consultation);
                    comm.Parameters.AddWithValue("@id_student", id_student);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows) result = true;
                    conn.Close();
                }
            }
            return result;
        }

        public bool HasRecord(int id_consultation, DateTime time)
        {
            bool result = false;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT id FROM Record WHERE id_consultation = @id_consultation AND start_time = @time";
                    comm.Parameters.AddWithValue("@id_consultation", id_consultation);
                    comm.Parameters.AddWithValue("@time", time.TimeOfDay);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    if (r.HasRows) result = true;
                    conn.Close();
                }
            }
            return result;
        }

        public List<string> GetStudentRecords(int id)
        {
            List<string> result = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT subject, surname + ' ' + name + ' ' + patronymic, CONVERT(varchar, date_time, 105) AS date, start_time, id_consultation FROM Record INNER JOIN Consultation ON Record.id_consultation = Consultation.id INNER JOIN Teacher ON id_teacher = Teacher.id WHERE id_student = @id ORDER BY date, start_time";
                    comm.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        result.Add(r[0].ToString() + '_' + r[1].ToString() + '_' + r[2].ToString() + '_' + r[3].ToString() + '_' + r[4].ToString());
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public void DeleteRecord(int id_consultation, int id_student)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Record WHERE id_consultation = @ic AND id_student = @is";
                    comm.Parameters.AddWithValue("@ic", id_consultation);
                    comm.Parameters.AddWithValue("@is", id_student);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public List<string []> GetConsultations(int id_teacher)
        {
            List<string []> result = new List<string []>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "SELECT DISTINCT Consultation.id, subject, date_time, duration_minutes, faculty + '-' + CONVERT(nvarchar(max),  grade) + N'Б' FROM Consultation INNER JOIN Timetable ON Consultation.id = id_consultation WHERE id_teacher = @id";
                    comm.Parameters.AddWithValue("@id", id_teacher);
                    conn.Open();
                    SqlDataReader r = comm.ExecuteReader();
                    while (r.Read())
                    {
                        string [] part = { r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString() };
                        result.Add(part);
                    }
                    conn.Close();
                }
            }
            for (int i = 0; i < result.Count; i++)
            {
                for (int j = i + 1; j < result.Count; j++)
                {
                    if (result[i][0] == result[j][0])
                    {
                        result[i][4] += "; " + result[j][4];
                        result.RemoveAt(j);
                        j--;
                    }
                }
            }
            return result;
        }

        public void DeleteConsultation(int id_consultation)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Consultation WHERE id = @ic";
                    comm.Parameters.AddWithValue("@ic", id_consultation);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public void Clear()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = conn.CreateCommand())
                {
                    comm.CommandType = System.Data.CommandType.Text;
                    comm.CommandText = "DELETE FROM Consultation WHERE date_time < @now";
                    comm.Parameters.AddWithValue("@now", DateTime.Now);
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
}
