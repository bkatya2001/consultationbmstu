﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    public partial class Consultation_form : Form
    {
        TeacherMenu_form form;
        public Consultation_form()
        {
            InitializeComponent();
        }

        public Consultation_form(TeacherMenu_form _form)
        {
            InitializeComponent();
            form = _form;
        }

        private void UpdateTable()
        {
            table.Rows.Clear();
            List<string[]> data = Program.database.GetConsultations(Program.teacher.id);
            foreach (var d in data)
            {
                table.Rows.Add(d[1], d[2].Split(' ')[0], d[2].Split(' ')[1].Remove(5), DateTime.Parse(d[2]).AddMinutes(Convert.ToInt32(d[3])).ToString().Split(' ')[1].Remove(5), d[4], d[0]);
            }
        }

        private void Consultation_form_Load(object sender, EventArgs e)
        {
            var subject_col = new DataGridViewColumn();
            var start_col = new DataGridViewColumn();
            var date_col = new DataGridViewColumn();
            var finish_col = new DataGridViewColumn();
            var group_col = new DataGridViewColumn();
            var id_col = new DataGridViewColumn();
            subject_col.HeaderText = "Дисциплина";
            subject_col.CellTemplate = new DataGridViewTextBoxCell();
            start_col.HeaderText = "Начало";
            start_col.CellTemplate = new DataGridViewTextBoxCell();
            date_col.HeaderText = "Дата";
            date_col.CellTemplate = new DataGridViewTextBoxCell();
            finish_col.HeaderText = "Конец";
            finish_col.CellTemplate = new DataGridViewTextBoxCell();
            group_col.HeaderText = "Группы";
            group_col.CellTemplate = new DataGridViewTextBoxCell();
            id_col.Visible = false;
            id_col.CellTemplate = new DataGridViewTextBoxCell();
            table.Columns.Add(subject_col);
            table.Columns.Add(date_col);
            table.Columns.Add(start_col);
            table.Columns.Add(finish_col);
            table.Columns.Add(group_col);
            table.Columns.Add(id_col);
            table.AllowUserToAddRows = false;
            UpdateTable();
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            if (table.CurrentRow != null)
            {
                var result = MessageBox.Show("Вы действительно хотите удалить консультацию?", "Удаление", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Program.database.DeleteConsultation(Convert.ToInt32(table.Rows[table.CurrentRow.Index].Cells[5].Value));
                    MessageBox.Show("Консультация удалена.", "Удаление", MessageBoxButtons.OK);
                    UpdateTable();
                }
            }
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            form.Show();
            this.Close();
        }
    }
}
