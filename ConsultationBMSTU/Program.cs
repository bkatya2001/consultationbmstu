﻿using ConsultationBMSTU.UserClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    static class Program
    {
        public static Student student;
        public static Teacher teacher;
        public static ConsultationBMSTUDataSet database;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            database = new ConsultationBMSTUDataSet();
            Thread thread = new Thread(ClearDatabase);
            thread.Start();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Auth_form());
        }

        public static int GetSem(int year)
        {
            int sem;
            if (DateTime.Now < new DateTime(year + 1, 1, 31)) sem = 1;
            else if (DateTime.Now < new DateTime(year + 1, 8, 31)) sem = 2;
            else if (DateTime.Now < new DateTime(year + 2, 1, 31)) sem = 3;
            else if (DateTime.Now < new DateTime(year + 2, 8, 31)) sem = 4;
            else if (DateTime.Now < new DateTime(year + 3, 1, 31)) sem = 5;
            else if (DateTime.Now < new DateTime(year + 3, 8, 31)) sem = 6;
            else if (DateTime.Now < new DateTime(year + 4, 1, 31)) sem = 7;
            else sem = 8;
            return sem;
        }

        private static void ClearDatabase()
        {
            database.Clear();
            Thread.Sleep(10000);
        }
    }
}
