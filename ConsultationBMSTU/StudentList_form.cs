﻿using ConsultationBMSTU.UserClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultationBMSTU
{
    public partial class StudentList_form : Form
    {
        string subject;
        int teacher;
        int consultation;
        string time;
        List<string> teachers;
        List<string> dates;

        StudentMenu_form form;
        public StudentList_form()
        {
            InitializeComponent();
        }

        public StudentList_form(StudentMenu_form _form)
        {
            InitializeComponent();
            form = _form;
        }

        private void time_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            time = time_cb.SelectedItem.ToString();
        }

        private void date_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            time_cb.Hide();
            time_lbl.Hide();
            time = null;
            time_cb.Items.Clear();
            consultation = Convert.ToInt32(dates[date_cb.SelectedIndex].Split('_')[0]);
            List<string> records = Program.database.GetRecords(consultation);
            DateTime start_time = DateTime.Parse(dates[date_cb.SelectedIndex].Split('_')[1]);
            DateTime current_time = start_time;
            DateTime pointer;
            List<DateTime> times = new List<DateTime>();
            while (current_time <= start_time.AddMinutes(Convert.ToInt32(dates[date_cb.SelectedIndex].Split('_')[2]) - 10))
            {
                if (records.Count > 0) pointer = DateTime.Parse(dates[date_cb.SelectedIndex].Split('_')[1].Split(' ')[0] + ' ' + records[0].Split('_')[0]);
                else pointer = start_time.AddMinutes(Convert.ToInt32(dates[date_cb.SelectedIndex].Split('_')[2]));
                while (current_time.AddMinutes(10) <= pointer)
                {
                    times.Add(current_time);
                    current_time = current_time.AddMinutes(10);
                }
                if (records.Count > 0)
                {
                    current_time = pointer.AddMinutes(Convert.ToInt32(records[0].Split('_')[1]));
                    records.RemoveAt(0);
                }
                else current_time = pointer;
            }
            foreach (var t in times) time_cb.Items.Add(t.ToString().Split(' ')[1].Remove(5));
            time_lbl.Show();
            time_cb.Show();
        }

        private void teacher_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            date_cb.Hide();
            date_lbl.Hide();
            consultation = -1;
            time_cb.Hide();
            time_lbl.Hide();
            time = null;
            date_cb.Items.Clear();
            teacher = Convert.ToInt32(teachers[teacher_cb.SelectedIndex].Split('-')[0]);
            date_lbl.Show();
            consultation = -1;
            dates = Program.database.GetDates(teacher, subject, Program.student.faculty, Program.GetSem(Program.student.year) * 10 + Program.student.group_num);
            foreach (var d in dates) date_cb.Items.Add(d.Split('_')[1].Split(' ')[0]);
            date_cb.Show();
        }

        private void subject_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            date_cb.Hide();
            date_lbl.Hide();
            consultation = -1;
            dates = null;
            time_cb.Hide();
            time_lbl.Hide();
            time = null;
            teacher_cb.Hide();
            teacher_lbl.Hide();
            teacher_cb.Items.Clear();
            subject = subject_cb.SelectedItem.ToString();
            teacher_lbl.Show();
            teacher = -1;
            teachers = Program.database.GetTeachers(subject, Program.student.faculty, Program.GetSem(Program.student.year) * 10 + Program.student.group_num);
            foreach (var t in teachers) teacher_cb.Items.Add(t.Split('-')[1]);
            teacher_cb.Show();
        }

        private void StudentList_form_Load(object sender, EventArgs e)
        {
            subject = null;
            teacher = -1;
            consultation = -1;
            time = null;
            teachers = null;
            dates = null;
            teacher_cb.Hide();
            teacher_lbl.Hide();
            date_cb.Hide();
            date_lbl.Hide();
            time_cb.Hide();
            time_lbl.Hide();
            List<string> subjects = Program.database.GetSubjects(Program.student.faculty, Program.GetSem(Program.student.year) * 10 + Program.student.group_num);
            foreach (var s in subjects) subject_cb.Items.Add(s);
        }

        private void record_btn_Click(object sender, EventArgs e)
        {
            if (Program.database.HasRecord(consultation, Program.student.id)) MessageBox.Show("Вы уже записывались на эту консультацию.", "Отказ", MessageBoxButtons.OK);
            else
            {
                if (time != null && consultation != -1)
                {
                    if (Program.database.HasRecord(consultation, DateTime.Parse(time))) MessageBox.Show("Кто-то записался на это время раньше Вас!", "Отказ", MessageBoxButtons.OK);
                    else
                    {
                        Program.database.AddRecord(consultation, Program.student.id, DateTime.Parse(time), 10);
                        MessageBox.Show("Запись произведена успешно!", "Успешно", MessageBoxButtons.OK);
                    }
                }
                else MessageBox.Show("Необходимо заполнить все поля.", "Отказ", MessageBoxButtons.OK);
            }
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            form.Show();
            this.Close();
        }
    }
}
