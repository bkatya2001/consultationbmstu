﻿namespace ConsultationBMSTU
{
    partial class Create_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Create_form));
            this.subject_lbl = new System.Windows.Forms.Label();
            this.duration_tb = new System.Windows.Forms.TextBox();
            this.subject_tb = new System.Windows.Forms.TextBox();
            this.exit_btn = new System.Windows.Forms.Button();
            this.date_lbl = new System.Windows.Forms.Label();
            this.time_lbl = new System.Windows.Forms.Label();
            this.duration_lbl = new System.Windows.Forms.Label();
            this.repeat_cb = new System.Windows.Forms.CheckedListBox();
            this.repeat_lbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.create_btn = new System.Windows.Forms.Button();
            this.date_mtb = new System.Windows.Forms.MaskedTextBox();
            this.time_mtb = new System.Windows.Forms.MaskedTextBox();
            this.group_lb = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // subject_lbl
            // 
            this.subject_lbl.AutoSize = true;
            this.subject_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.subject_lbl.Location = new System.Drawing.Point(53, 16);
            this.subject_lbl.Name = "subject_lbl";
            this.subject_lbl.Size = new System.Drawing.Size(142, 22);
            this.subject_lbl.TabIndex = 0;
            this.subject_lbl.Text = "Дисциплина:";
            // 
            // duration_tb
            // 
            this.duration_tb.Location = new System.Drawing.Point(201, 165);
            this.duration_tb.Name = "duration_tb";
            this.duration_tb.Size = new System.Drawing.Size(250, 26);
            this.duration_tb.TabIndex = 6;
            this.duration_tb.Text = "Минуты";
            this.duration_tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // subject_tb
            // 
            this.subject_tb.Location = new System.Drawing.Point(201, 12);
            this.subject_tb.Name = "subject_tb";
            this.subject_tb.Size = new System.Drawing.Size(250, 26);
            this.subject_tb.TabIndex = 9;
            this.subject_tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // exit_btn
            // 
            this.exit_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.exit_btn.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit_btn.Location = new System.Drawing.Point(12, 398);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(84, 34);
            this.exit_btn.TabIndex = 11;
            this.exit_btn.Text = "Назад";
            this.exit_btn.UseVisualStyleBackColor = false;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // date_lbl
            // 
            this.date_lbl.AutoSize = true;
            this.date_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.date_lbl.Location = new System.Drawing.Point(125, 67);
            this.date_lbl.Name = "date_lbl";
            this.date_lbl.Size = new System.Drawing.Size(70, 22);
            this.date_lbl.TabIndex = 12;
            this.date_lbl.Text = "Дата:";
            // 
            // time_lbl
            // 
            this.time_lbl.AutoSize = true;
            this.time_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.time_lbl.Location = new System.Drawing.Point(113, 118);
            this.time_lbl.Name = "time_lbl";
            this.time_lbl.Size = new System.Drawing.Size(82, 22);
            this.time_lbl.TabIndex = 13;
            this.time_lbl.Text = "Время:";
            // 
            // duration_lbl
            // 
            this.duration_lbl.AutoSize = true;
            this.duration_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.duration_lbl.Location = new System.Drawing.Point(29, 169);
            this.duration_lbl.Name = "duration_lbl";
            this.duration_lbl.Size = new System.Drawing.Size(166, 22);
            this.duration_lbl.TabIndex = 14;
            this.duration_lbl.Text = "Длительность:";
            // 
            // repeat_cb
            // 
            this.repeat_cb.BackColor = System.Drawing.Color.LightGray;
            this.repeat_cb.CheckOnClick = true;
            this.repeat_cb.FormattingEnabled = true;
            this.repeat_cb.Items.AddRange(new object[] {
            "Никогда",
            "Каждую неделю",
            "Каждые две недели",
            "Каждые четыре недели"});
            this.repeat_cb.Location = new System.Drawing.Point(33, 257);
            this.repeat_cb.Name = "repeat_cb";
            this.repeat_cb.Size = new System.Drawing.Size(221, 96);
            this.repeat_cb.TabIndex = 15;
            // 
            // repeat_lbl
            // 
            this.repeat_lbl.AutoSize = true;
            this.repeat_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.repeat_lbl.Location = new System.Drawing.Point(29, 221);
            this.repeat_lbl.Name = "repeat_lbl";
            this.repeat_lbl.Size = new System.Drawing.Size(130, 22);
            this.repeat_lbl.TabIndex = 16;
            this.repeat_lbl.Text = "Повторять:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(297, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 22);
            this.label2.TabIndex = 17;
            this.label2.Text = "Группы:";
            // 
            // create_btn
            // 
            this.create_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.create_btn.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.create_btn.Location = new System.Drawing.Point(352, 383);
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(114, 49);
            this.create_btn.TabIndex = 19;
            this.create_btn.Text = "Создать";
            this.create_btn.UseVisualStyleBackColor = false;
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // date_mtb
            // 
            this.date_mtb.BeepOnError = true;
            this.date_mtb.Location = new System.Drawing.Point(201, 63);
            this.date_mtb.Mask = "00/00/0000";
            this.date_mtb.Name = "date_mtb";
            this.date_mtb.Size = new System.Drawing.Size(250, 26);
            this.date_mtb.TabIndex = 20;
            this.date_mtb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.date_mtb.ValidatingType = typeof(System.DateTime);
            // 
            // time_mtb
            // 
            this.time_mtb.Location = new System.Drawing.Point(201, 114);
            this.time_mtb.Mask = "00:00";
            this.time_mtb.Name = "time_mtb";
            this.time_mtb.Size = new System.Drawing.Size(250, 26);
            this.time_mtb.TabIndex = 21;
            this.time_mtb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.time_mtb.ValidatingType = typeof(System.DateTime);
            // 
            // group_lb
            // 
            this.group_lb.FormattingEnabled = true;
            this.group_lb.ItemHeight = 20;
            this.group_lb.Location = new System.Drawing.Point(301, 257);
            this.group_lb.Name = "group_lb";
            this.group_lb.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.group_lb.Size = new System.Drawing.Size(120, 84);
            this.group_lb.TabIndex = 22;
            // 
            // Create_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(478, 444);
            this.Controls.Add(this.group_lb);
            this.Controls.Add(this.time_mtb);
            this.Controls.Add(this.date_mtb);
            this.Controls.Add(this.create_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.repeat_lbl);
            this.Controls.Add(this.repeat_cb);
            this.Controls.Add(this.duration_lbl);
            this.Controls.Add(this.time_lbl);
            this.Controls.Add(this.date_lbl);
            this.Controls.Add(this.exit_btn);
            this.Controls.Add(this.subject_tb);
            this.Controls.Add(this.duration_tb);
            this.Controls.Add(this.subject_lbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "Create_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Создать консультацию";
            this.Load += new System.EventHandler(this.Create_form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label subject_lbl;
        private System.Windows.Forms.TextBox duration_tb;
        private System.Windows.Forms.TextBox subject_tb;
        private System.Windows.Forms.Button exit_btn;
        private System.Windows.Forms.Label date_lbl;
        private System.Windows.Forms.Label time_lbl;
        private System.Windows.Forms.Label duration_lbl;
        private System.Windows.Forms.CheckedListBox repeat_cb;
        private System.Windows.Forms.Label repeat_lbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button create_btn;
        private System.Windows.Forms.MaskedTextBox date_mtb;
        private System.Windows.Forms.MaskedTextBox time_mtb;
        private System.Windows.Forms.ListBox group_lb;
    }
}