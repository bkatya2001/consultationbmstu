﻿namespace ConsultationBMSTU
{
    partial class TeacherMenu_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TeacherMenu_form));
            this.do_btn = new System.Windows.Forms.Button();
            this.show_btn = new System.Windows.Forms.Button();
            this.exit_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // do_btn
            // 
            this.do_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.do_btn.Font = new System.Drawing.Font("Courier New", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.do_btn.Location = new System.Drawing.Point(35, 38);
            this.do_btn.Name = "do_btn";
            this.do_btn.Size = new System.Drawing.Size(400, 100);
            this.do_btn.TabIndex = 2;
            this.do_btn.Text = "Создать консультацию";
            this.do_btn.UseVisualStyleBackColor = false;
            this.do_btn.Click += new System.EventHandler(this.do_btn_Click);
            // 
            // show_btn
            // 
            this.show_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.show_btn.Font = new System.Drawing.Font("Courier New", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.show_btn.Location = new System.Drawing.Point(35, 174);
            this.show_btn.Name = "show_btn";
            this.show_btn.Size = new System.Drawing.Size(400, 100);
            this.show_btn.TabIndex = 3;
            this.show_btn.Text = "Мои консультации";
            this.show_btn.UseVisualStyleBackColor = false;
            this.show_btn.Click += new System.EventHandler(this.show_btn_Click);
            // 
            // exit_btn
            // 
            this.exit_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.exit_btn.Font = new System.Drawing.Font("Courier New", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit_btn.Location = new System.Drawing.Point(35, 303);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(400, 100);
            this.exit_btn.TabIndex = 4;
            this.exit_btn.Text = "Выйти";
            this.exit_btn.UseVisualStyleBackColor = false;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // TeacherMenu_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(478, 444);
            this.Controls.Add(this.exit_btn);
            this.Controls.Add(this.show_btn);
            this.Controls.Add(this.do_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "TeacherMenu_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Меню";
            this.Load += new System.EventHandler(this.TeacherMenu_form_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button do_btn;
        private System.Windows.Forms.Button show_btn;
        private System.Windows.Forms.Button exit_btn;
    }
}