﻿namespace ConsultationBMSTU
{
    partial class StudentList_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentList_form));
            this.label1 = new System.Windows.Forms.Label();
            this.subject_cb = new System.Windows.Forms.ComboBox();
            this.subject_lbl = new System.Windows.Forms.Label();
            this.teacher_lbl = new System.Windows.Forms.Label();
            this.teacher_cb = new System.Windows.Forms.ComboBox();
            this.date_lbl = new System.Windows.Forms.Label();
            this.date_cb = new System.Windows.Forms.ComboBox();
            this.time_lbl = new System.Windows.Forms.Label();
            this.time_cb = new System.Windows.Forms.ComboBox();
            this.record_btn = new System.Windows.Forms.Button();
            this.exit_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выберите консультацию и время:";
            // 
            // subject_cb
            // 
            this.subject_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subject_cb.FormattingEnabled = true;
            this.subject_cb.Location = new System.Drawing.Point(160, 66);
            this.subject_cb.MaxDropDownItems = 100;
            this.subject_cb.Name = "subject_cb";
            this.subject_cb.Size = new System.Drawing.Size(296, 28);
            this.subject_cb.TabIndex = 1;
            this.subject_cb.SelectedIndexChanged += new System.EventHandler(this.subject_cb_SelectedIndexChanged);
            // 
            // subject_lbl
            // 
            this.subject_lbl.AutoSize = true;
            this.subject_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.subject_lbl.Location = new System.Drawing.Point(12, 72);
            this.subject_lbl.Name = "subject_lbl";
            this.subject_lbl.Size = new System.Drawing.Size(142, 22);
            this.subject_lbl.TabIndex = 2;
            this.subject_lbl.Text = "Дисциплина:";
            // 
            // teacher_lbl
            // 
            this.teacher_lbl.AutoSize = true;
            this.teacher_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.teacher_lbl.Location = new System.Drawing.Point(12, 128);
            this.teacher_lbl.Name = "teacher_lbl";
            this.teacher_lbl.Size = new System.Drawing.Size(178, 22);
            this.teacher_lbl.TabIndex = 3;
            this.teacher_lbl.Text = "Преподаватель:";
            // 
            // teacher_cb
            // 
            this.teacher_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teacher_cb.FormattingEnabled = true;
            this.teacher_cb.Location = new System.Drawing.Point(196, 122);
            this.teacher_cb.MaxDropDownItems = 100;
            this.teacher_cb.Name = "teacher_cb";
            this.teacher_cb.Size = new System.Drawing.Size(260, 28);
            this.teacher_cb.TabIndex = 4;
            this.teacher_cb.SelectedIndexChanged += new System.EventHandler(this.teacher_cb_SelectedIndexChanged);
            // 
            // date_lbl
            // 
            this.date_lbl.AutoSize = true;
            this.date_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.date_lbl.Location = new System.Drawing.Point(12, 186);
            this.date_lbl.Name = "date_lbl";
            this.date_lbl.Size = new System.Drawing.Size(70, 22);
            this.date_lbl.TabIndex = 5;
            this.date_lbl.Text = "Дата:";
            // 
            // date_cb
            // 
            this.date_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.date_cb.FormattingEnabled = true;
            this.date_cb.Location = new System.Drawing.Point(88, 180);
            this.date_cb.MaxDropDownItems = 100;
            this.date_cb.Name = "date_cb";
            this.date_cb.Size = new System.Drawing.Size(368, 28);
            this.date_cb.TabIndex = 6;
            this.date_cb.SelectedIndexChanged += new System.EventHandler(this.date_cb_SelectedIndexChanged);
            // 
            // time_lbl
            // 
            this.time_lbl.AutoSize = true;
            this.time_lbl.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.time_lbl.Location = new System.Drawing.Point(12, 243);
            this.time_lbl.Name = "time_lbl";
            this.time_lbl.Size = new System.Drawing.Size(82, 22);
            this.time_lbl.TabIndex = 7;
            this.time_lbl.Text = "Время:";
            // 
            // time_cb
            // 
            this.time_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.time_cb.FormattingEnabled = true;
            this.time_cb.Location = new System.Drawing.Point(100, 237);
            this.time_cb.MaxDropDownItems = 100;
            this.time_cb.Name = "time_cb";
            this.time_cb.Size = new System.Drawing.Size(356, 28);
            this.time_cb.TabIndex = 8;
            this.time_cb.SelectedIndexChanged += new System.EventHandler(this.time_cb_SelectedIndexChanged);
            // 
            // record_btn
            // 
            this.record_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.record_btn.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.record_btn.Location = new System.Drawing.Point(308, 354);
            this.record_btn.Name = "record_btn";
            this.record_btn.Size = new System.Drawing.Size(158, 78);
            this.record_btn.TabIndex = 9;
            this.record_btn.Text = "Записаться";
            this.record_btn.UseVisualStyleBackColor = false;
            this.record_btn.Click += new System.EventHandler(this.record_btn_Click);
            // 
            // exit_btn
            // 
            this.exit_btn.BackColor = System.Drawing.Color.MediumTurquoise;
            this.exit_btn.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit_btn.Location = new System.Drawing.Point(12, 383);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(103, 49);
            this.exit_btn.TabIndex = 10;
            this.exit_btn.Text = "Назад";
            this.exit_btn.UseVisualStyleBackColor = false;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // StudentList_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(478, 444);
            this.Controls.Add(this.exit_btn);
            this.Controls.Add(this.record_btn);
            this.Controls.Add(this.time_cb);
            this.Controls.Add(this.time_lbl);
            this.Controls.Add(this.date_cb);
            this.Controls.Add(this.date_lbl);
            this.Controls.Add(this.teacher_cb);
            this.Controls.Add(this.teacher_lbl);
            this.Controls.Add(this.subject_lbl);
            this.Controls.Add(this.subject_cb);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "StudentList_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Записаться";
            this.Load += new System.EventHandler(this.StudentList_form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox subject_cb;
        private System.Windows.Forms.Label subject_lbl;
        private System.Windows.Forms.Label teacher_lbl;
        private System.Windows.Forms.ComboBox teacher_cb;
        private System.Windows.Forms.Label date_lbl;
        private System.Windows.Forms.ComboBox date_cb;
        private System.Windows.Forms.Label time_lbl;
        private System.Windows.Forms.ComboBox time_cb;
        private System.Windows.Forms.Button record_btn;
        private System.Windows.Forms.Button exit_btn;
    }
}